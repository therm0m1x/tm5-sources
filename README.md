# tm5-sources

The Thermomix TM5 GPL/LGPL sources obtained by Vorwerk.

## Where to get?

Just write an email to opensource@vorwerk.de and request the sources.
